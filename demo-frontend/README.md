# Configuracion de frontend

Este proyecto fue creado con el framework de angular 

## Requisitos
Para poder trabajar con este proyecto debemos tener instalado 
- nodejs 
## Development server

Para levantar el server de desarrollo tenemos que ejecutar los siguientes comandos 

**Install dependencies:**

```
npm install
```

**Levantar el servidor de angular:**

```
npm start
```