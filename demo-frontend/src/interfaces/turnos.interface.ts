export interface Area {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}

export interface Type {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}

export interface Tickets {
  id: number;
  names: string;
  observations: string;
  attended: boolean;
  created_at: Date;
  updated_at: Date;
  area: Area;
  type: Type;
}