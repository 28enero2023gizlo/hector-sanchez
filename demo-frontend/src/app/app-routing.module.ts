import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/*
import { AssessmentComponent } from './components/assessment/assessment.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { QuestionComponent } from './components/question/question.component';
import { ParticipantComponent } from './components/participant/participant.component';
*/
import { TurnoComponent } from 'src/Components/turno/turno.component';

const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: 'login', component: LoginComponent }
  { path: 'IngresaTurno', component: TurnoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
