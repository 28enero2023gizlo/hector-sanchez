import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
  import { Subscription } from 'rxjs';
  import { ActivatedRoute, Router } from '@angular/router';
  import { TurnosService } from 'src/services/turnos/turno.services';
  import { AreasService } from 'src/services/areas/areas.services';
  import { TypesService } from 'src/services/tipos/tipos.services';
  import {
    Area,
    Tickets,
    Type,
  } from 'src/interfaces/turnos.interface';
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
  
  @Component({ selector: 'app-turno', templateUrl: './turno.component.html'
  //, styleUrls: ['./turno.component.scss']
   })
  export class TurnoComponent implements OnInit,
    AfterViewInit {
    public page = 1;
    public pageSize = 10;
    public assessmentLength = 0;
    filterPost = '';
    public tickets: Tickets[] = [];
    public areas: Area[] = [];
    public types: Type[] = [];
    formSave: FormGroup;
  
    constructor(private router: Router, public turnosService :TurnosService,public typesService : TypesService, public areasService: AreasService, private _formBuilder: FormBuilder,) {
      this.formSave = this._formBuilder.group({
        names: ['', Validators.required],
        area: ['', Validators.required],
        type: ['', Validators.required],
        observations: ['', Validators.required]
      });
  
    }
    ngAfterViewInit(): void {
      //throw new Error('Method not implemented.');
    }
  
    ngOnInit(): void {
        this.getAllTickets();
        this.getAllAreas();
        this.getAllTypes();
    }
  
    public getAllTypes(): void {
      this.typesService.getAllTypes().subscribe(res => {
        this.types = res as Type[];
      });
    }
    public getAllTickets(): void {
      this.turnosService.getAllTicket().subscribe(res => {
        this.tickets = res as Tickets[];
      });
    }

    public getAllAreas(): void {
      this.areasService.getAllAreas().subscribe(res => {
        this.areas = res as Area[];
      }); 
    }
  
    send(){
      if (this.formSave.valid) {
        console.log(this.formSave.value);
        this.turnosService.createNewTicket(this.formSave.value).subscribe(
          (res ) => {

            this.getAllTickets();
            this.formSave.reset();  
          }
          
          );
      } else {
        alert('Debe llenar todos los campos');
      }
    }
  
  }