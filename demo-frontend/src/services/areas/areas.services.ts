import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Area } from '../../interfaces/turnos.interface';
import { Parameter } from '../parameter.entity';
import { ServiceGenerator } from '../base/service-generator';

@Injectable({ providedIn: 'root' })
export class AreasService extends ServiceGenerator {

    constructor(private http: HttpClient) {
        super();
    }

    getAllAreas(): Observable<Area[]> {
        return this.http.get<Area[]>(this.buildurl('/areas/all'));
    }

  
}
