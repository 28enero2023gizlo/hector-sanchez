import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tickets } from '../../interfaces/turnos.interface';
import { Parameter } from '../parameter.entity';
import { ServiceGenerator } from '../base/service-generator';

@Injectable({ providedIn: 'root' })
export class TurnosService extends ServiceGenerator {

    constructor(private http: HttpClient) {
        super();
    }

    getAllTicket(): Observable<Tickets[]> {
        return this.http.get<Tickets[]>(this.buildurl('/tickets/all'));
    }

    createNewTicket({ area, type, ...json} : any): Observable<Tickets> {
          return this.http.post<Tickets>(this.buildurl(`/area/${area}/type/${type}/tickets`),json);
      }
}
