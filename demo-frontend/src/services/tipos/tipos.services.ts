import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Type } from '../../interfaces/turnos.interface';
import { Parameter } from '../parameter.entity';
import { ServiceGenerator } from '../base/service-generator';

@Injectable({ providedIn: 'root' })
export class TypesService extends ServiceGenerator {

    constructor(private http: HttpClient) {
        super();
    }

    getAllTypes(): Observable<Type[]> {
        return this.http.get<Type[]>(this.buildurl('/types/all'));
    }

  
}
