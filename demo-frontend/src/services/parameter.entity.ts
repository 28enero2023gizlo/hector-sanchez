
export class Parameter{
  clave: string;
  valor: any;

  constructor(key: string, value: any){
      this.clave = key;
      this.valor = value;
  }

  static buildParamsUlr(lista: Parameter[]): string{
    let finalParamsUrl = '';
    let counter = 0;
    lista.forEach(element => {
      if (counter === 0) {
        finalParamsUrl = finalParamsUrl + '?' + element.clave + '=' + element.valor;
      }
      else {
        finalParamsUrl = finalParamsUrl + '&' + element.clave + '=' + element.valor;
      }
      counter++;
    });
    return finalParamsUrl;
  }

  static existParam(elem: Parameter, list: Parameter[]): boolean{
     const keylist: string[] = [];
     list.forEach(element => {
        keylist.push(element.clave);
     });
     return keylist.includes(elem.clave);

  }

  static addParameter(elem: Parameter, list: Parameter[]): Parameter[]{
    const listaclaves: string[] = [];
    list.forEach(element => {
       listaclaves.push(element.clave);
    });

    if (!listaclaves.includes(elem.clave)){
        list.push(elem);
    }
    else{
      // tslint:disable-next-line:only-arrow-functions
      list = list.filter((obj: Parameter) => {
        return obj.clave !== elem.clave;
      });
      list.push(elem);
    }
    return list;
  }

  static removerParametro(elem: Parameter, list: Parameter[]): Parameter[]{
    // tslint:disable-next-line:only-arrow-functions
    list = list.filter((obj: Parameter) => {
      return obj.clave !== elem.clave;
    });
    return list;
  }

  static remove(clave: string, lista: Parameter[]): void {
      const index: number = lista.findIndex(x => x.clave === clave);
      if (index >= 0) {
          lista.splice(index);
      }
  }


  static add(lista: Parameter[], clave: string, valor: any): void{
    const parametro = new Parameter(clave, valor);
    if (valor != null){
      Parameter.remove(clave, lista);
      Parameter.addParameter(parametro, lista);
    }
  }
}
