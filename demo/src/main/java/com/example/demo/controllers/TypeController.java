package com.example.demo.controllers;

import com.example.demo.models.TypeModel;
import com.example.demo.services.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class TypeController {
    @Autowired
    TypeService typeService;
    @GetMapping("/types/all")
    public ArrayList<TypeModel> getAllTypes() {
        return this.typeService.getAll();
    }

    @PostMapping("/types/add")
    public TypeModel createArea(@RequestBody TypeModel type) {
        return this.typeService.saveType(type);
    }

}
