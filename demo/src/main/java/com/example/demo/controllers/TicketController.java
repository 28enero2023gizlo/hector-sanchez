package com.example.demo.controllers;

import com.example.demo.models.AreaModel;
import com.example.demo.models.TicketModel;
import com.example.demo.models.TypeModel;
import com.example.demo.services.AreaService;
import com.example.demo.services.TicketService;
import com.example.demo.services.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TicketController {
    @Autowired
    TypeService typeService;

    @Autowired
    TicketService ticketService;

    @Autowired
    AreaService areaService;

    @GetMapping("/tickets/all")
    public ResponseEntity<TicketModel> getAllTickets() {
        try {
            ArrayList<TicketModel> tickets = this.ticketService.getAll();
            return new ResponseEntity(tickets, HttpStatus.OK);
        }catch (DataAccessException e){
            return new ResponseEntity("{\"error\": \"Error getting tickets\"}", HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/tickets/history")
    public ResponseEntity<TicketModel> getHistoryTickets() {
        try {
            ArrayList<TicketModel> tickets = this.ticketService.getHistory();
            return new ResponseEntity(tickets, HttpStatus.OK);
        }catch (DataAccessException e){
            return new ResponseEntity("{\"error\": \"Error getting tickets\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/area/{areaId}/type/{typeId}/tickets")
    public ResponseEntity<TicketModel> createTicket(@PathVariable(value="areaId") Long areaId, @PathVariable(value="typeId") Long typeId, @RequestBody TicketModel newTicket) {
        try {
            System.out.println("AreaId: " + areaId);
            Optional<AreaModel> area = this.areaService.getById(areaId);
            Optional<TypeModel> type = this.typeService.getById(typeId);
            if (!area.isPresent() || !type.isPresent()) {
                return new ResponseEntity("{\"error\": \"Area or Type not found\"}", HttpStatus.NOT_FOUND);
            }
            newTicket.setType(type.get());
            newTicket.setArea(area.get());
            TicketModel ticket = this.ticketService.saveTicket(newTicket);
            return new ResponseEntity(ticket, HttpStatus.CREATED);
        }catch (DataAccessException e){
            return new ResponseEntity("{\"error\": \"Error creating ticket\"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
