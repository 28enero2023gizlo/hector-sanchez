package com.example.demo.controllers;

import com.example.demo.models.AreaModel;
import com.example.demo.services.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class AreaController {
    @Autowired
    AreaService areaService;
    @GetMapping("/areas/all")
    public ArrayList<AreaModel> getAllAreas() {
        return this.areaService.getAll();
    }

    @PostMapping("/areas/add")
    public AreaModel createArea(@RequestBody AreaModel area) {
        return this.areaService.saveArea(area);
    }
}
