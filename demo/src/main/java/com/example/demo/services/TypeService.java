package com.example.demo.services;

import com.example.demo.models.TypeModel;
import com.example.demo.repositories.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
@Service
public class TypeService {
    @Autowired
    TypeRepository typeRepository;

    public ArrayList<TypeModel> getAll(){
        return (ArrayList<TypeModel>) typeRepository.findAll();
    }

    public Optional<TypeModel> getById(Long typeId){
        return typeRepository.findById(typeId);
    }
    public TypeModel saveType(TypeModel newType){
        return typeRepository.save(newType);
    }
}
