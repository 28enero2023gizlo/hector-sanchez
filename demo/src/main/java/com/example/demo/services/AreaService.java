package com.example.demo.services;

import com.example.demo.models.AreaModel;
import com.example.demo.repositories.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class AreaService {
    @Autowired
    AreaRepository areaRepository;

    public ArrayList<AreaModel> getAll(){
        return (ArrayList<AreaModel>) areaRepository.findAll();
    }

    public Optional<AreaModel> getById(Long areaId){
        return areaRepository.findById(areaId);
    }
    public AreaModel saveArea(AreaModel newArea){
        return areaRepository.save(newArea);
    }

}
