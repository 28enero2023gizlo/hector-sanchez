package com.example.demo.services;

import com.example.demo.models.TicketModel;
import com.example.demo.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TicketService {
    @Autowired
    TicketRepository ticketRepository;

    public ArrayList<TicketModel>getAll(){
        // return array list with area information from database with the attended is false
        return (ArrayList<TicketModel>) ticketRepository.findAllUnattended();
    }

    public ArrayList<TicketModel>getHistory(){
        // return array list with area information from database with the attended is true
        return (ArrayList<TicketModel>) ticketRepository.findAttended();
    }

    public TicketModel saveTicket(TicketModel ticket) {
        return ticketRepository.save(ticket);
    }

}
