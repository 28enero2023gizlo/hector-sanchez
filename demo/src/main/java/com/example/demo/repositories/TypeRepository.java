package com.example.demo.repositories;

import com.example.demo.models.TypeModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends CrudRepository<TypeModel, Long> {

}
