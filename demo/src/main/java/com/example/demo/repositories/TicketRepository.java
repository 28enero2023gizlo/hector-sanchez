package com.example.demo.repositories;

import com.example.demo.models.TicketModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<TicketModel, Long> {
    @Query("SELECT t FROM tickets t WHERE attended = false")
    List<TicketModel> findAllUnattended();

    @Query("SELECT t FROM tickets t WHERE attended = true")
    List<TicketModel> findAttended();
}
