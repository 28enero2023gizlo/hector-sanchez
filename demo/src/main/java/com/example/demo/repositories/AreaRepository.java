package com.example.demo.repositories;

import com.example.demo.models.AreaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaRepository extends CrudRepository<AreaModel, Long> {
}
