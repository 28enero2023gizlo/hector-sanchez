-- Insert for the areas table
INSERT INTO tickets.areas (id,created_at,name,updated_at) VALUES
	 (1,'2023-01-28 09:57:51.406442','Sistemas','2023-01-28 09:57:51.406495'),
	 (2,'2023-01-28 09:58:21.884060','Financiero','2023-01-28 09:58:21.884083'),
	 (3,'2023-01-28 09:58:28.620053','RRHH','2023-01-28 09:58:28.620076');

-- Insert for the types table
INSERT INTO tickets.types (id,created_at,name,updated_at) VALUES
	 (1,'2023-01-28 12:09:07.668078','Retiro de equipos','2023-01-28 12:09:07.668126'),
	 (2,'2023-01-28 12:09:16.492744','Entrega de equipos','2023-01-28 12:09:16.492779'),
	 (3,'2023-01-28 12:09:23.057145','Otros','2023-01-28 12:09:23.057175');
