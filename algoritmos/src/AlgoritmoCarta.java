import java.util.Random;

public class AlgoritmoCarta {
    public static void main(String[] args) {
        Random random = new Random();
        //se setean los 4 tipos de cartas que existen en una baraja de masos, tambien llamados (palos)
        String[] palos = {"CORAZON_NEGRO", "CORAZON_ROJO", "DIAMANTE", "TREBOL"};
        // se setean las 4 letras que existen en maso
        String[] figuras = {"J", "Q", "K", "AS"};
        String cartaAnterior = "";
        String paloAnterior = "";

        for (int i = 0; i < 10; i++) {
            int numeroAleatorio = random.nextInt(52);
            String palo = "";
            String valor = "";

            // Determine el palo de la carta
            if (paloAnterior.equals("")) {
                // Es la primera carta, así que seleccionar cualquier palo
                palo = palos[random.nextInt(4)];
            } else {
                // Seleccionar un palo diferente al anterior
                do {
                    palo = palos[random.nextInt(4)];
                } while (palo.equals(paloAnterior));
            }

            // Determine el valor de la carta
            if (cartaAnterior.equals("")) {
                // Es la primera carta, así que seleccionar cualquier valor
                valor = Integer.toString(random.nextInt(10) + 1);
            } else if (Character.isDigit(cartaAnterior.charAt(0))) {
                // La carta anterior era un número, así que seleccionar una figura
                valor = figuras[random.nextInt(3)];
            } else {
                // La carta anterior era una figura, así que seleccionar un número
                valor = Integer.toString(random.nextInt(10) + 1);
            }
            
            	
            //1 por el as
            if(valor.equals("1") ) {
            	valor = "AS";
            }

            // Guardar la carta y el palo para la siguiente iteración
            cartaAnterior = valor;
            paloAnterior = palo;
            
            

            // Mostrar la carta generada
            System.out.println(valor + " de " + palo);
        }
    }
}
