import java.util.Scanner;

public class IdentificacionValidator {
    public static void main(String[] args) {
        System.out.println ("Ingresa tu número de cédula: ");
        Scanner entradaEscaner = new Scanner(System.in); //Creación de un objeto Scanner
        String identificacion = entradaEscaner.nextLine(); //Invocamos un método sobre un objeto Scanner
        System.out.println ("La identificación ingresada es \"" + identificacion + "\"");
        validarIdentificacion(identificacion);
    }

    public static void validarIdentificacion(String identificacion) {
        if (identificacion.length() == 10 ) {
        	ValidacionCiudad(identificacion);
            return;
        }
        if (identificacion.length() == 13) {
            System.out.println("La identificación es un RUC");
            return;
        }
        System.out.println("La identificación no es válida");
    }
    
    public static void ValidacionCiudad(String NumeroCiudad) {
        if (!NumeroCiudad.startsWith("09")){
            System.out.println("La identificación es una Cédula y es de Otra Provincia del Ecuador");
            return;
        }
        System.out.println("La identificación es una Cédula y pertenece a la provincia del Guayas");
    }
}
